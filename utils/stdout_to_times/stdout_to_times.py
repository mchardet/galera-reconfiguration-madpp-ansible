import sys
import re
from json import dumps

pattern = re.compile("Total time in seconds: (.+) \(deploy: (.+), reconf: (.+)\)")

with open(sys.argv[1]) as file:
    for line in reversed(list(file)):
        line = line.rstrip()
        match = pattern.search(line)
        if match:
            total_time = float(match.group(1))
            total_deploy_time = float(match.group(2))
            total_reconf_time = float(match.group(3))
            print(dumps({"total_time": total_time, "total_deploy_time": total_deploy_time, "total_reconf_time": total_reconf_time}))
            break
        
