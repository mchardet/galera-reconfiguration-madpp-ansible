import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import json
import math
import statistics
from matplotlib.backends.backend_pdf import PdfPages

FRAMEWORKS = ["concerto", "aeolus", "ansible", "overhead_concerto_nothing", "dryrun_concerto", "overhead_aeolus_nothing", "dryrun_aeolus"]
NB_ADD_NODES = [1, 5, 10, 20]
#NB_ENTRIES = [1, 1000, 50000, 1000000]
NB_ENTRIES = [1000]

def percentile(N, percent, key=lambda x:x):
    """
    Find the percentile of a list of values.

    @parameter N - is a list of values. Note N MUST BE already sorted.
    @parameter percent - a float value from 0.0 to 1.0.
    @parameter key - optional key function to compute value from each element of N.

    @return - the percentile of the values
    """
    if not N:
        return None
    k = (len(N)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return key(N[int(k)])
    d0 = key(N[int(f)]) * (c-k)
    d1 = key(N[int(c)]) * (k-f)
    return d0+d1


# Init reconf data
reconf_data = {}

for framework in FRAMEWORKS:
    reconf_data[framework] = {}
    for nbad in NB_ADD_NODES:
        reconf_data[framework][nbad] = {}
        for nent in NB_ENTRIES:
            with open("%s-nb_db_3+%d-nb_ent_%d.json"%(framework, nbad, nent)) as f:
                result = json.load(f)
            values = sorted(result['total_reconf2_time']['points'])
            if len(values) is 0:
                print("Warning: no data for %s, %d, %d!"%(framework,nbad,nent))
                reconf_data[framework][nbad][nent] = {
                    'mean': 0,
                    'sd': 0,
                    'min': 0,
                    'max': 0
                }
                continue
            elif len(values) < 6:
                print("Warning: less than 6 points (%d) for %s, %d, %d!"%(len(values),framework,nbad,nent))
            median = result['total_reconf2_time']['median']
            q1 = percentile(values, 0.25)
            q3 = percentile(values, 0.75)
            filtered_values = list(filter(lambda x: x >= q1-(median-q1) and x <= q3+(q3-median), values))
            if len(filtered_values) < 6:
                print("Warning: after filter, less than 6 points (%d) for %s, %d, %d!"%(len(filtered_values),framework,nbad,nent))
            filtered_mean = statistics.mean(filtered_values)
            if len(filtered_values) > 1:
                filtered_sd = statistics.stdev(filtered_values, filtered_mean)
            else:
                filtered_sd = 0.
            filtered_min = min(filtered_values)
            filtered_max = max(filtered_values)
            reconf_data[framework][nbad][nent] = {
                'mean': filtered_mean,
                'sd': filtered_sd,
                'min': filtered_min,
                'max': filtered_max
            }
            
def fixed_nb_entries(nbe):

    fig, axs = plt.subplots(1)

    axs.errorbar(NB_ADD_NODES,
                [reconf_data['concerto'][i][nbe]['mean'] for i in NB_ADD_NODES],
                [reconf_data['concerto'][i][nbe]['sd'] for i in NB_ADD_NODES],
                None,
                'r-o',
                label='Concerto')
    axs.errorbar(NB_ADD_NODES,
                [reconf_data['aeolus'][i][nbe]['mean'] for i in NB_ADD_NODES],
                [reconf_data['aeolus'][i][nbe]['sd'] for i in NB_ADD_NODES],
                None,
                'g-x',
                label='Aeolus')
    axs.errorbar(NB_ADD_NODES,
                [reconf_data['ansible'][i][nbe]['mean'] for i in NB_ADD_NODES],
                [reconf_data['ansible'][i][nbe]['sd'] for i in NB_ADD_NODES],
                None,
                'b-+',
                label='Ansible')
    axs.plot(NB_ADD_NODES,
                [reconf_data['concerto'][i][nbe]['mean']-reconf_data['overhead_concerto_nothing'][i][nbe]['mean']+reconf_data['dryrun_concerto'][i][nbe]['mean'] for i in NB_ADD_NODES],
                'r--',
                label='No-ov-concerto')
    axs.plot(NB_ADD_NODES,
                [reconf_data['aeolus'][i][nbe]['mean']-reconf_data['overhead_aeolus_nothing'][i][nbe]['mean']+reconf_data['dryrun_aeolus'][i][nbe]['mean'] for i in NB_ADD_NODES],
                'g--',
                label='No-ov-aeolus')
    
    plt.xlabel('Number of additional nodes')
    plt.ylabel('Execution time (s)')
    plt.xticks(NB_ADD_NODES,NB_ADD_NODES)
    legend = plt.legend(loc='upper left', shadow=True, prop={'size': 11})

    axs.set_xlim(xmin=0)
    axs.set_ylim(ymin=0, ymax=60)

    pp = PdfPages('figure2.pdf')
    plt.savefig(pp, format='pdf', bbox_inches='tight')
    pp.close()
    
    for i in NB_ADD_NODES:
        print("====== Number of nodes: %d ======"%i)
        print("Ansible      : %f" % reconf_data['ansible'][i][nbe]['mean'])
        print("Aeolus       : %f" % reconf_data['aeolus'][i][nbe]['mean'])
        print("Concerto     : %f" % reconf_data['concerto'][i][nbe]['mean'])
        print("Aeolus (no)  : %f" % (reconf_data['aeolus'][i][nbe]['mean']-reconf_data['overhead_aeolus_nothing'][i][nbe]['mean']+reconf_data['dryrun_aeolus'][i][nbe]['mean']))
        print("  overhead: %f" % reconf_data['overhead_aeolus_nothing'][i][nbe]['mean'])
        print("  dryrun  : %f" % reconf_data['dryrun_aeolus'][i][nbe]['mean'])
        ans = reconf_data['ansible'][i][nbe]['mean']
        ano = reconf_data['aeolus'][i][nbe]['mean']-reconf_data['overhead_aeolus_nothing'][i][nbe]['mean']+reconf_data['dryrun_aeolus'][i][nbe]['mean']
        print("  gain to Ansible: %f (%f%%)" % (ans-ano, (ans-ano)*100./ans))
        print("Concerto (no): %f" % (reconf_data['concerto'][i][nbe]['mean']-reconf_data['overhead_concerto_nothing'][i][nbe]['mean']+reconf_data['dryrun_concerto'][i][nbe]['mean']))
        print("  overhead: %f" % reconf_data['overhead_concerto_nothing'][i][nbe]['mean'])
        print("  dryrun  : %f" % reconf_data['dryrun_concerto'][i][nbe]['mean'])
        ans = reconf_data['ansible'][i][nbe]['mean']
        cno = reconf_data['concerto'][i][nbe]['mean']-reconf_data['overhead_concerto_nothing'][i][nbe]['mean']+reconf_data['dryrun_concerto'][i][nbe]['mean']
        print("  gain to Ansible: %f (%f%%)" % (ans-cno, (ans-cno)*100./ans))
        print("  gain to Aeolus : %f (%f%%)" % (ano-cno, (ano-cno)*100./ano))

    #plt.show()

fixed_nb_entries(1000)
