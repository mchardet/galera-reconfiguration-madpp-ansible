for ndb in 3 5 10 20; do
  for nent in 1000; do
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/concerto_nanr/exp/concerto-nanr-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > concerto-nanr-nb_db_${ndb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/aeolus_nanr/exp/aeolus-nanr-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > aeolus-nanr-nb_db_${ndb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/ansible_nr/exp/ansible-nr-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > ansible-nr-nb_db_${ndb}-nb_ent_${nent}.json
  done
done

for nadb in 1 5 10 20; do
  for nent in 1000; do
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/concerto_nanr/exp/concerto-nanr-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > concerto-nanr-nb_db_3+${nadb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/aeolus_nanr/exp/aeolus-nanr-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > aeolus-nanr-nb_db_3+${nadb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/ansible_nr/exp/ansible-nr-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > ansible-nr-nb_db_3+${nadb}-nb_ent_${nent}.json
  done
done
