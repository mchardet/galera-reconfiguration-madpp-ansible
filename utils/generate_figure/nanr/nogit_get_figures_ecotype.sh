mv ecotype_figure0.pdf bak/ecotype_figure0.bak.pdf
mv ecotype_figure1.pdf bak/ecotype_figure1.bak.pdf
mv ecotype_figure2.pdf bak/ecotype_figure2.bak.pdf
mv ecotype_figure0_data.json bak/ecotype_figure0_data.bak.json
mv ecotype_figure1_data.json bak/ecotype_figure1_data.bak.json
mv ecotype_figure2_data.json bak/ecotype_figure2_data.bak.json
mv ecotype_figure0_analysis.txt bak/ecotype_figure0_analysis.bak.txt
mv ecotype_figure1_analysis.txt bak/ecotype_figure1_analysis.bak.txt
mv ecotype_figure2_analysis.txt bak/ecotype_figure2_analysis.bak.txt
mv ecotype_durations.json bak/ecotype_durations.bak.json
mv ecotype_figure1bis.pdf bak/ecotype_figure1bis.bak.pdf
mv ecotype_figure2bis.pdf bak/ecotype_figure2bis.bak.pdf
mv ecotype_figure1bis_data.json bak/ecotype_figure1bis_data.bak.json
mv ecotype_figure2bis_data.json bak/ecotype_figure2bis_data.bak.json
ssh nantes.g5k << EOF
  cd galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/; 
  bash generate_figures.sh
EOF
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure0.pdf ecotype_figure0.pdf
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure0_data.json ecotype_figure0_data.json
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure0_analysis.txt ecotype_figure0_analysis.txt
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure1.pdf ecotype_figure1.pdf
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure1_data.json ecotype_figure1_data.json
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure1_analysis.txt ecotype_figure1_analysis.txt
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure2.pdf ecotype_figure2.pdf
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure2_data.json ecotype_figure2_data.json
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure2_analysis.txt ecotype_figure2_analysis.txt
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/durations.json ecotype_durations.json
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure1bis.pdf ecotype_figure1bis.pdf
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure1bis_data.json ecotype_figure1bis_data.json
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure2bis.pdf ecotype_figure2bis.pdf
scp nantes.g5k:galera-reconfiguration-madpp-ansible/utils/generate_figure/nanr/figure2bis_data.json ecotype_figure2bis_data.json
