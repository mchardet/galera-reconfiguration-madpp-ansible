import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import json
import sys
from matplotlib.backends.backend_pdf import PdfPages

if len(sys.argv) < 3:
    print("Syntax: %s <figure data file> <figure name> (<legend size> (<y_max> (<no_y_min>)))" % sys.argv[0])
    exit(1)

figure_data_file = sys.argv[1]
figure_name = sys.argv[2]
legend_size = int(sys.argv[3]) if len(sys.argv) > 3 else 16
y_max = int(sys.argv[4]) if len(sys.argv) > 4 else 0
no_y_min = len(sys.argv) > 5


with open(figure_data_file) as f:
    figure_data = json.load(f)

mapping = {
    "concerto-noansible": {"line": 'r-o', "label": "Concerto"},
    "aeolus-noansible": {"line": 'g-x', "label": "Aeolus"},
    "ansible": {"line": 'b-+', "label": "Ansible"},
    "predicted-concerto": {"line": 'r--', "label": "Concerto (theo)"},
    "predicted-aeolus": {"line": 'g--', "label": "Aeolus (theo)"}
}


fig, axs = plt.subplots(1)

for framework in sorted(figure_data.keys()):
    fdata = figure_data[framework]
    nb_nodes = sorted(map(lambda x: int(x), fdata.keys()))
    axs.errorbar(
        nb_nodes,
        [fdata[str(i)]['mean'] for i in nb_nodes],
        [fdata[str(i)]['sd'] for i in nb_nodes],
        None,
        mapping[framework]["line"],
        label=mapping[framework]["label"]
    )

plt.xlabel('Number of nodes')
plt.ylabel('Execution time (s)')
plt.xticks(nb_nodes,nb_nodes)
legend = plt.legend(loc='upper left', shadow=True, prop={'size': legend_size})

axs.set_xlim(xmin=0)
if not no_y_min:
    axs.set_ylim(ymin=0)
if y_max != 0:
    axs.set_ylim(ymax=y_max)

pp = PdfPages('%s.pdf'%figure_name)
plt.savefig(pp, format='pdf', bbox_inches='tight')
pp.close()

#plt.show()
