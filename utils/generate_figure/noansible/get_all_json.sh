for ndb in 3 5 10 20; do
  for nent in 1000; do
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/concerto_noansible/exp/concerto-noansible-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > concerto-noansible-nb_db_${ndb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/aeolus_noansible/exp/aeolus-noansible-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > aeolus-noansible-nb_db_${ndb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/ansible/exp/ansible-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > ansible-nb_db_${ndb}-nb_ent_${nent}.json
  done
done

for nadb in 1 5 10 20; do
  for nent in 1000; do
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/concerto_noansible/exp/concerto-noansible-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > concerto-noansible-nb_db_3+${nadb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/aeolus_noansible/exp/aeolus-noansible-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > aeolus-noansible-nb_db_3+${nadb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-reconfiguration-madpp-ansible/ansible/exp/ansible-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > ansible-nb_db_3+${nadb}-nb_ent_${nent}.json
  done
done
