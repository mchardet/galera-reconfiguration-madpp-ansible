import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import json
import math
import statistics
from matplotlib.backends.backend_pdf import PdfPages

FIGURE_NAME = "figure2"

FRAMEWORKS = ["concerto-noansible", "aeolus-noansible", "ansible"]
NB_ADD_NODES = [1, 5, 10, 20]
#NB_ENTRIES = [1, 1000, 50000, 1000000]
NB_ENTRIES = [1000]

def percentile(N, percent, key=lambda x:x):
    """
    Find the percentile of a list of values.

    @parameter N - is a list of values. Note N MUST BE already sorted.
    @parameter percent - a float value from 0.0 to 1.0.
    @parameter key - optional key function to compute value from each element of N.

    @return - the percentile of the values
    """
    if not N:
        return None
    k = (len(N)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return key(N[int(k)])
    d0 = key(N[int(f)]) * (c-k)
    d1 = key(N[int(c)]) * (k-f)
    return d0+d1


# Init reconf data
reconf_data = {}

for framework in FRAMEWORKS:
    reconf_data[framework] = {}
    for nbad in NB_ADD_NODES:
        reconf_data[framework][nbad] = {}
        for nent in NB_ENTRIES:
            with open("%s-nb_db_3+%d-nb_ent_%d.json"%(framework, nbad, nent)) as f:
                result = json.load(f)
            values = sorted(result['total_reconf2_time']['points'])
            if len(values) is 0:
                print("Warning: no data for %s, %d, %d!"%(framework,nbad,nent))
                reconf_data[framework][nbad][nent] = {
                    'mean': 0,
                    'sd': 0,
                    'min': 0,
                    'max': 0
                }
                continue
            elif len(values) < 6:
                print("Warning: less than 6 points (%d) for %s, %d, %d!"%(len(values),framework,nbad,nent))
            median = result['total_reconf2_time']['median']
            q1 = percentile(values, 0.25)
            q3 = percentile(values, 0.75)
            filtered_values = list(filter(lambda x: x >= q1-(median-q1) and x <= q3+(q3-median), values))
            if len(filtered_values) < 6:
                print("Warning: after filter, less than 6 points (%d) for %s, %d, %d!"%(len(filtered_values),framework,nbad,nent))
            filtered_mean = statistics.mean(filtered_values)
            if len(filtered_values) > 1:
                filtered_sd = statistics.stdev(filtered_values, filtered_mean)
            else:
                filtered_sd = 0.
            filtered_min = min(filtered_values)
            filtered_max = max(filtered_values)
            reconf_data[framework][nbad][nent] = {
                'mean': filtered_mean,
                'sd': filtered_sd,
                'min': filtered_min,
                'max': filtered_max,
                'values': filtered_values,
                'discarded_values': list(filter(lambda x: x not in filtered_values, values))
            }
            
def fixed_nb_entries(nbe):
    dict_figure = dict()
    for f in FRAMEWORKS:
        fr_data = dict()
        for i in NB_ADD_NODES:
            fr_data[i] = reconf_data[f][i][nbe]
        dict_figure[f] = fr_data
    
    with open("%s_data.json"%FIGURE_NAME, "w") as f:
        json.dump(dict_figure, f, indent="\t")
    
    analysis = ""
    
    for i in NB_ADD_NODES:
        analysis += "%s\n" % ("====== Number of nodes added: %d ======"%i)
        analysis += "%s\n" %("Ansible      : %f" % reconf_data['ansible'][i][nbe]['mean'])
        analysis += "%s\n" %("Aeolus       : %f" % reconf_data['aeolus-noansible'][i][nbe]['mean'])
        ans = reconf_data['ansible'][i][nbe]['mean']
        ano = reconf_data['aeolus-noansible'][i][nbe]['mean']
        analysis += "%s\n" %("  gain to Ansible: %f (%f%%)" % (ans-ano, (ans-ano)*100./ans))
        analysis += "%s\n" %("Concerto     : %f" % reconf_data['concerto-noansible'][i][nbe]['mean'])
        ans = reconf_data['ansible'][i][nbe]['mean']
        cno = reconf_data['concerto-noansible'][i][nbe]['mean']
        analysis += "%s\n" %("  gain to Ansible: %f (%f%%)" % (ans-cno, (ans-cno)*100./ans))
        analysis += "%s\n" %("  gain to Aeolus : %f (%f%%)" % (ano-cno, (ano-cno)*100./ano))
        analysis += "\n"
    
    print(analysis)
    with open("%s_analysis.txt"%FIGURE_NAME, "w") as f:
        f.write(analysis)

    #plt.show()

fixed_nb_entries(1000)
