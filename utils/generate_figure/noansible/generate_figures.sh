rm *.json
bash get_all_json.sh
python3 generate_figure0_data.py
python3 generate_figure1_data.py
python3 generate_figure2_data.py
python3 json_to_figure.py figure0_data.json "figure0" 11 0 "noymin"
python3 json_to_figure.py figure1_data.json "figure1" 11 0 "noymin"
python3 json_to_figure.py figure2_data.json "figure2" 11 60
python3 get_transitions_durations.py > durations.json
python3 get_figure1bis_data.py > figure1bis_data.json
python3 get_figure2bis_data.py > figure2bis_data.json
python3 json_to_figure.py figure1bis_data.json "figure1bis" 11 0 "noymin"
python3 json_to_figure.py figure2bis_data.json "figure2bis" 11 60
