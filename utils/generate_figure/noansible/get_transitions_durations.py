import json
import subprocess
import sys
import statistics
        


def pack_transitions_durations(target_times, pattern, times_key, keep_latest):
    try:
        command = 'ls -1d %s'%pattern
        print("Running command:%s\n"%command, file=sys.stderr)
        output = bytes.decode(subprocess.check_output(command, shell=True))
        print("Output:\n%s\n"%output, file=sys.stderr)
        folders = filter(lambda x: len(x.strip()) > 0, output.split("\n"))
    except:
        folders = []
        print ("No directories to process", file=sys.stderr)
        return None
            
    transition_times = {
        "master": dict(),
        "workers": dict(),
        "adworkers": dict()
    }

    for exp_folder in folders:
        try:
            with open("%stimes.json"%exp_folder) as f:
                times = json.load(f)
            if times[times_key] not in target_times:
                continue
            with open("%sresults.json"%exp_folder) as f:
                results = json.load(f)
            
            for comp_trans in results["transitions"]:
                comp_name = comp_trans["component"]
                if comp_name.endswith("_mariadb"):
                    transitions = dict()
                    for b in comp_trans["behaviors"]:
                        for t in b["transitions"]:
                            t_name = t["transition"]
                            if t_name not in transitions or (keep_latest and t["start"] > transitions[t_name]["start"]) or (not keep_latest and t["start"] < transitions[t_name]["start"]):
                                transitions[t_name] = {
                                    "start": t["start"],
                                    "end": t["end"]
                                }
                    pool = "workers"
                    if comp_name == "master_mariadb":
                        pool = "master"
                    elif comp_name.startswith("adworker"):
                        pool = "adworkers"
                    for t_name, t_times in transitions.items():
                        if t_name not in transition_times[pool]:
                            transition_times[pool][t_name] = {
                                "values": []
                            }
                        transition_times[pool][t_name]["values"].append(t_times["end"]-t_times["start"])
            
        except:
            print ("Skipped %s because an exception was raised"%exp_folder, file=sys.stderr)
            
    for pool,times_dict in transition_times.items():
        for t_name, trans_dict in times_dict.items():
            trans_dict["mean"] = statistics.mean(trans_dict["values"]) if trans_dict["values"] else None
            trans_dict["median"] = statistics.median(trans_dict["values"]) if trans_dict["values"] else None
            trans_dict["min"] = min(trans_dict["values"]) if trans_dict["values"] else None
            trans_dict["max"] = max(trans_dict["values"]) if trans_dict["values"] else None
    return transition_times



def get_transitions_durations(figure_json, incomplete_pattern, times_key, keep_latest):
    total_times = {}
    with open(figure_json) as f:
        for nb_cl, details in json.load(f)["aeolus-noansible"].items():
            total_times[nb_cl] = details["values"]
    
    trans_durations = dict()
    for nb_cl, target_times in total_times.items():
        trans_durations[nb_cl] = pack_transitions_durations(
            target_times,
            incomplete_pattern%nb_cl,
            times_key,
            keep_latest
        )
    
    return trans_durations




param_sets = [
    {
        "figure_json": "figure0_data.json",
        "incomplete_pattern": "~/galera-reconfiguration-madpp-ansible/aeolus_noansible/exp/aeolus-noansible-nb_db_%s+*-nb_ent_1000-*/",
        "times_key": "total_reconf_time",
        "key": "reconf0",
        "keep_latest": False
    },
    {
        "figure_json": "figure1_data.json",
        "incomplete_pattern": "~/galera-reconfiguration-madpp-ansible/aeolus_noansible/exp/aeolus-noansible-nb_db_%s+*-nb_ent_1000-*/",
        "times_key": "total_reconf_time",
        "key": "reconf1",
        "keep_latest": True
    },
    {
        "figure_json": "figure2_data.json",
        "incomplete_pattern": "~/galera-reconfiguration-madpp-ansible/aeolus_noansible/exp/aeolus-noansible-nb_db_3+%s-nb_ent_1000-*/",
        "times_key": "total_reconf2_time",
        "key": "reconf2",
        "keep_latest": True
    }
]

times = dict()
for ps in param_sets:
    times[ps["key"]] = get_transitions_durations(ps["figure_json"], ps["incomplete_pattern"], ps["times_key"], ps["keep_latest"])

print (json.dumps(times, indent="\t"))
