for ndb in 3 5 10 20; do
  for nent in 1000; do
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/concerto/exp/concerto-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > concerto-nb_db_${ndb}-nb_ent_${nent}.json
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/aeolus/exp/aeolus-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > aeolus-nb_db_${ndb}-nb_ent_${nent}.json
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/ansible/exp/ansible-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > ansible-nb_db_${ndb}-nb_ent_${nent}.json
    
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/overhead_concerto_nothing/exp/overhead_concerto_nothing-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > overhead_concerto_nothing-nb_db_${ndb}-nb_ent_${nent}.json
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/overhead_concerto_nothing/exp/dryrun_concerto-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > dryrun_concerto-nb_db_${ndb}-nb_ent_${nent}.json
    
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/overhead_concerto_critical/exp/overhead_concerto_critical-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > overhead_concerto_critical-nb_db_${ndb}-nb_ent_${nent}.json
    
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/overhead_aeolus_nothing/exp/overhead_aeolus_nothing-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > overhead_aeolus_nothing-nb_db_${ndb}-nb_ent_${nent}.json
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/overhead_aeolus_nothing/exp/dryrun_aeolus-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > dryrun_aeolus-nb_db_${ndb}-nb_ent_${nent}.json
  done
done

for nadb in 1 5 10 20; do
  for nent in 1000; do
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/concerto/exp/concerto-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > concerto-nb_db_3+${nadb}-nb_ent_${nent}.json
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/aeolus/exp/aeolus-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > aeolus-nb_db_3+${nadb}-nb_ent_${nent}.json
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/ansible/exp/ansible-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > ansible-nb_db_3+${nadb}-nb_ent_${nent}.json
    
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/overhead_concerto_nothing/exp/overhead_concerto_nothing-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > overhead_concerto_nothing-nb_db_3+${nadb}-nb_ent_${nent}.json
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/overhead_concerto_nothing/exp/dryrun_concerto-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > dryrun_concerto-nb_db_3+${nadb}-nb_ent_${nent}.json
    
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/overhead_concerto_critical/exp/overhead_concerto_critical-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > overhead_concerto_critical-nb_db_3+${nadb}-nb_ent_${nent}.json
    
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/overhead_aeolus_nothing/exp/overhead_aeolus_nothing-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > overhead_aeolus_nothing-nb_db_3+${nadb}-nb_ent_${nent}.json
    python3 ~/galera-reconfiguration-madpp-ansible/utils/group_times_results/group_times_results.py "~/galera-reconfiguration-madpp-ansible/overhead_aeolus_nothing/exp/dryrun_aeolus-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > dryrun_aeolus-nb_db_3+${nadb}-nb_ent_${nent}.json
  done
done
