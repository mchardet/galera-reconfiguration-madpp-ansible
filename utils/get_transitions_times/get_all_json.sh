for nadb in 1 5 10 20; do
  for nent in 1000; do
    python3 ~/galera-reconfiguration-madpp-ansible/utils/get_transitions_times/get_transitions_times.py "~/galera-reconfiguration-madpp-ansible/concerto/exp/concerto-nb_db_3+${nadb}-nb_ent_${nent}-*/" > concerto-nb_db_3+${nadb}-nb_ent_${nent}.json
  done
done

python3 ~/galera-reconfiguration-madpp-ansible/utils/get_transitions_times/nice_all.py
