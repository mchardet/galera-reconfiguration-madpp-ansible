import sys
import subprocess
from json import load, dumps
import math
import statistics
from copy import deepcopy

from filter_outliers import filter_outliers


def get_transitions_times(pattern, no_overhead=True):
    metrics = ["use_docker", "mount", "pull", "create_directories", "send_config", "start", "go_ready"]

    metric_dict = {
        "mean": 0.,
        "median": 0.,
        "variance": 0.,
        "standard_dev": 0.,
        "min": math.inf,
        "max": 0.,
        "points": []
    }

    results = {
        "nb_points": 0,
        "points": [],
        "estimated_time": 0.
    }

    for m in metrics:
        results[m] = deepcopy(metric_dict)

    folders = filter_outliers(pattern)

    for exp_folder in folders:
        # print(line)
        try:
            with open("%sresults.json"%exp_folder) as f:
                times = load(f)
            for cb in times["transitions"]:
                if cb["component"].startswith("adworker") and cb["component"].endswith("_mariadb"):
                    results["nb_points"] += 1
                    #results["points"].append(cb["behaviors"][0])
                    for tb in cb["behaviors"][0]["transitions"]:
                        if tb["transition"] in metrics:
                            time = tb["end"]-tb["start"]
                            results[tb["transition"]]["points"].append(time)
                        else:
                            print("%s not in metrics!"%tb["transition"], file=sys.stderr)
            
        except:
            print ("Skipped %s because an exception was raised"%exp_folder, file=sys.stderr)

    def compute_agregation(res_dict):
        res_dict["mean"] = statistics.mean(res_dict["points"])
        res_dict["median"] = statistics.median(res_dict["points"])
        if len(res_dict["points"]) > 1:
            res_dict["variance"] = statistics.variance(res_dict["points"], res_dict["mean"])
            res_dict["standard_dev"] = math.sqrt(res_dict["variance"])
        res_dict["min"] = min(res_dict["points"])
        res_dict["max"] = max(res_dict["points"])

    if results["nb_points"] > 0:
        for m in metrics:
            compute_agregation(results[m])
    
    overhead = 0.
    if no_overhead:
        overhead = (12.549597967066662 - 0.028040973299998485)/6. # Measured on Aeolus 3+1, TODO: Remove hardcoded
    dd = results["use_docker"]["mean"] - overhead
    dp = results["pull"]["mean"] - overhead
    dm = results["mount"]["mean"] - overhead
    dc = results["create_directories"]["mean"] - overhead
    dsc= results["send_config"]["mean"] - overhead
    dst= results["start"]["mean"] - overhead
    dg = results["go_ready"]["mean"] - overhead
    dr = 0. # Not in the worker version of MariaDB
    results["estimated_time"] = max(dd+dp, dm+dc+dsc) + dst+dg+dr


    return results


if __name__ == '__main__':
    pattern = sys.argv[1]
    results = get_transitions_times(pattern, no_overhead=False)
    print(dumps(results, indent='\t'))

