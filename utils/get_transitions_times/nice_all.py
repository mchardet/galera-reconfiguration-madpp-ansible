import json

for nadb in [1, 5, 10, 20]:
    for nent in [1000]:
        with open("concerto-nb_db_3+%s-nb_ent_%s.json"%(nadb, nent)) as f:
            times = json.load(f)
        print("%s,%s: %f"%(nadb, nent, times["estimated_time"]))
        
