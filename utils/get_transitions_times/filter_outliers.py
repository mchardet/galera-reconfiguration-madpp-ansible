import json
import math
import statistics
import sys
import subprocess
from json import load, dumps
from copy import deepcopy

def get_agreg(pattern):
    results = {
        "nb_points": 0,
        "points": []
    }
    try:
        command = 'ls -1d %s'%pattern
        print("Running command:%s\n"%command, file=sys.stderr)
        output = bytes.decode(subprocess.check_output(command, shell=True))
        print("Output:\n%s\n"%output, file=sys.stderr)
        folders = filter(lambda x: len(x.strip()) > 0, output.split("\n"))
    except:
        folders = []
        print ("No directories to process", file=sys.stderr)
        

    for exp_folder in folders:
        try:
            with open("%stimes.json"%exp_folder) as f:
                times = load(f)
        except:
            print ("Skipped %s because an exception was raised"%exp_folder, file=sys.stderr)
            
        results["nb_points"] += 1
        time = times["total_reconf2_time"]
        results["points"].append({"time": time, "directory": exp_folder})
            
    if results["nb_points"] > 0:
        results["median"] = statistics.median([p["time"] for p in results["points"]])

    return results


def percentile(N, percent, key=lambda x:x):
    """
    Find the percentile of a list of values.

    @parameter N - is a list of values. Note N MUST BE already sorted.
    @parameter percent - a float value from 0.0 to 1.0.
    @parameter key - optional key function to compute value from each element of N.

    @return - the percentile of the values
    """
    if not N:
        return None
    k = (len(N)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return key(N[int(k)])
    d0 = key(N[int(f)]) * (c-k)
    d1 = key(N[int(c)]) * (k-f)
    return d0+d1


def filter_outliers(pattern):
    # Init reconf data
    reconf_data = {}
    
    agreg = get_agreg(pattern)
    
    values = sorted([p["time"] for p in agreg["points"]])
    if len(values) is 0:
        print("Warning: no values for pattern %s"%pattern, file=sys.stderr)
        return []
    median = agreg['median']
    q1 = percentile(values, 0.25)
    q3 = percentile(values, 0.75)
    filtered_points = list(filter(lambda x: x["time"] >= q1-(median-q1) and x["time"] <= q3+(q3-median), agreg["points"]))
    return list([p["directory"] for p in filtered_points])
