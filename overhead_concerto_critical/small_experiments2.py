import logging

from reserve_overhead_concerto_critical import perform_experiments

if __name__ == '__main__':
    #Testing
    logging.basicConfig(level=logging.DEBUG)
    perform_experiments(
        start_attempt=1,
        nb_attempts=10,
        nbs_nodes=[(3,1),(3,5)]
    )
