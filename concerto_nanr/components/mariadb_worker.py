import time

from concerto.all import *
from concerto.utility import *
from concerto.plugins.ansible import call_ansible_on_host, AnsibleCallResult

from components.cutils.remote_host import RemoteHost


class MariaDBWorker(Component):

    def __init__(self, host):
        self.host = host
        #self.remote_host_handler = RemoteHost(host['ip'], backend=RemoteHost.Backend.SSH)
        self.remote_host_handler = RemoteHost(host['ip'])
        self.pulled = False
        self.galera = False
        Component.__init__(self)

    def create(self):
        self.places = [
            'uninstalled',
            'scripts_sent',
            'ready_to_pull',
            'mounted',
            'directories_created',
            'configured_pulled',
            'started',
            'ready'
        ]
        
        self.groups = {
            'using_docker': ['ready_to_pull','configured_pulled','started','ready']
        }

        self.transitions = {
            'send_scripts': ('uninstalled', 'scripts_sent', 'install', 0, self.send_scripts),
            'mount': ('scripts_sent', 'mounted', 'install', 0, self.mount),
            'create_directories': ('mounted', 'directories_created', 'install', 0, self.create_directories),
            'send_config': ('directories_created', 'configured_pulled', 'install', 0, self.send_config),
            'use_docker': ('scripts_sent', 'ready_to_pull', 'install', 0, empty_transition),
            'pull': ('ready_to_pull', 'configured_pulled', 'install', 0, self.pull),
            'start': ('configured_pulled', 'started', 'install', 0, self.start),
            'go_ready': ('started', 'ready', 'install', 0, self.go_ready),
            'change_config': ('ready', 'ready', 'change_config', 0, self.change_config),
            'stop': ('ready', 'mounted', 'stop', 0, self.stop),
            'stop_uninstall': ('ready', 'mounted', 'uninstall', 0, self.stop),
            'uninstall': ('mounted', 'scripts_sent', 'uninstall', 0, self.uninstall)
        }

        self.dependencies = {
            'config': (DepType.DATA_USE, ['send_config', 'change_config']),
            'pip_libs': (DepType.USE, ['using_docker']),
            'docker': (DepType.USE, ['using_docker']),
            'master_mariadb': (DepType.USE, ['start']),
            'mariadb': (DepType.PROVIDE, ['ready'])
        }
        
        self.initial_place = 'uninstalled'
        
    def send_scripts(self):
        self.print_color("Sending scripts")
        self.remote_host_handler.send_files([
            "bash/database/backup.sh",
            "bash/database/destroy.sh",
            "bash/database/mkdir.sh",
            "bash/database/mount.sh",
            "bash/database/pull.sh",
            "bash/database/restore.sh",
            #"bash/database/run_db.sh",
            "bash/database/unmount.sh"
        ])
        #self.remote_host_handler.write_jinja2_file(
            #"bash/database/create_cluster.sh.j2",
            #{"db_host": self.host['ip']},
            #"./create_cluster.sh"
        #)
        self.remote_host_handler.write_jinja2_file(
            "bash/database/join_cluster.sh.j2",
            {"db_host": self.host['ip']},
            "./join_cluster.sh"
        )
        self.print_color("Sent scripts")
        
    def mount(self):
        self.print_color("Mounting")
        self.remote_host_handler.run("bash ./mount.sh")
        self.print_color("Mounted database dir")
        
    def create_directories(self):
        self.print_color("Creating directories")
        self.remote_host_handler.run("bash ./mkdir.sh")
        self.print_color("Created directories")
        
    def send_config(self):
        self.print_color("Seonding config")
        config = self.read("config")
        if config is "":
            self.print_color("Empty config, skipping send_config")
        else:
            self.galera = True
            self.print_color("Changing config to:\n%s"%config)
            self.remote_host_handler.write_file(config,"/database/mysql.conf.d/mysql_server.cnf")
            self.print_color("Sent config")
        
    def pull(self):
        self.print_color("Pulling image")
        if not self.pulled:
            self.print_color("Pulling image")
            self.remote_host_handler.run("bash ./pull.sh")
            self.print_color("Pulled image")
            self.pulled = True

    def start(self):
        self.print_color("Starting container")
        if self.galera:
            self.remote_host_handler.run("bash ./join_cluster.sh")
        else:
            raise Exception("MariaDB worker component is not tagged Galera, which should always be the case.")
        self.print_color("Started container")

    def go_ready(self):
        self.print_color("Going ready")
        self.remote_host_handler.wait_for_port(3306)
        self.print_color("Database ready")

    def change_config(self):
        self.print_color("Changing config")
        time.sleep(1) # TODO: check

    def stop(self):
        self.print_color("Stopping mariadb")
        self.remote_host_handler.run("bash ./destroy.sh")
        self.print_color("Stopped container")

    def uninstall(self):
        self.print_color("Uninstall mariadb")
        self.remote_host_handler.run("bash ./unmount.sh")
        self.print_color("Unmounted /database")

    def clear_image(self):
        time.sleep(1) # TODO: check

