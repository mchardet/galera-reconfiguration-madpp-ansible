import logging
import os

from reserve_concerto import perform_experiments

if __name__ == '__main__':
    #Testing
    logging.basicConfig(level=logging.DEBUG)
    perform_experiments(
        start_attempt=1,
        nb_attempts=15,
        nbs_nodes=[(3,1),(3,5)],
        sweeper_dir = os.path.join(os.getenv('HOME'), 'galera-concerto-fix-sweeper')
    )
