#!/usr/bin/env python

import copy
import logging
import os
import time
from pprint import pformat

import galera_ansible as ga
from execo_engine.sweep import (ParamSweeper, sweep)


DEFAULT_WORKING_DIRECTORY = '.'

def run_experiment(nb_db_entries, conf, working_directory=DEFAULT_WORKING_DIRECTORY, provider='g5k', force_deployment=True, destroy=False, reconf2=False):
    from execo.action import Put, Get, Remote
    from execo.host import Host
    from json import dump
    
    total_time, total_deploy_time, total_reconf_time, total_reconf2_time = ga.deploy(nb_db_entries, conf, provider, force_deployment, destroy=destroy, working_directory=working_directory)
    results = {"total_time": total_time, "total_deploy_time": total_deploy_time, "total_reconf_time": total_reconf_time}
    if reconf2:
        results["total_reconf2_time"] = total_reconf2_time
    with open(working_directory + "/times.json", "w") as results_file:
        dump(results, results_file)
    #stdout
    #stderr
    #results.gpl
    #results.json
    #data.sql
    
    
    
PREFIX_EXPERIMENT = "ansible"
DEFAULT_SWEEPER_NAME = "galera-ansible-sweeper"
DEFAULT_NBS_NODES = [(3,1),(3,5),(3,10),(3,20),(5,0),(10,0),(20,0)]
DEFAULT_NBS_ENTRIES = [1000]
DEFAULT_START_ATTEMPT = 1
DEFAULT_NB_ATTEMPTS = 10
DEFAULT_SWEEPER_DIR = os.path.join(os.getenv('HOME'), DEFAULT_SWEEPER_NAME)

def perform_experiments(start_attempt = DEFAULT_START_ATTEMPT, nb_attempts = DEFAULT_NB_ATTEMPTS, nbs_nodes = DEFAULT_NBS_NODES, nbs_entries = DEFAULT_NBS_ENTRIES, sweeper_dir = DEFAULT_SWEEPER_DIR):
    import copy, yaml, traceback
    from os import makedirs
    from execo_engine.sweep import (ParamSweeper, sweep)
    from pprint import pformat
    
    #CONF = [] #TO GET SOMEHOW
    with open("conf.yaml") as f:
        CONF = yaml.load(f)
    
    sweeper = ParamSweeper(
        sweeper_dir,
        sweeps=sweep({
            'nb_db_nodes': list(nbs_nodes),
            'nb_db_entries': list(nbs_entries),
            'attempt': list(range(start_attempt, start_attempt+nb_attempts))
        }))
    
    logging.info("Using Execo sweeper in directory: %s"%sweeper_dir)
    done_tasks = sweeper.get_done()
    if len(done_tasks) > 0:
        logging.info("Tasks already done:\n%s"%"\n".join("- " + pformat(task) for task in done_tasks))
    
    while sweeper.get_remaining():
        combination = sweeper.get_next()
        logging.info("Treating combination %s" % pformat(combination))

        try:
            # Setup parameters
            conf = copy.deepcopy(CONF)  # Make a deepcopy so we can run
                                        # multiple sweeps in parallels
            (nb_db_nodes,nb_additional_db_nodes) = combination['nb_db_nodes']
            nb_db_entries = combination['nb_db_entries']
            attempt = combination['attempt']
            conf['g5k']['resources']['machines'][0]['nodes'] = nb_db_nodes
            conf['g5k']['resources']['machines'][1]['nodes'] = nb_additional_db_nodes
            xp_name = "%s-nb_db_%d+%d-nb_ent_%d-%d" % (PREFIX_EXPERIMENT, nb_db_nodes, nb_additional_db_nodes, nb_db_entries, attempt)

            # Let's get it started!
            wd = "exp/%s"%xp_name
            makedirs(wd, exist_ok=True)
            with open(wd+'/g5k_config.yaml', 'w') as g5k_config_file:
                yaml.dump(conf, g5k_config_file)
            
            run_experiment(nb_db_entries, conf, wd, destroy=False, reconf2=(nb_additional_db_nodes>0))

            # Everything works well, mark combination as done
            sweeper.done(combination)
            logging.info("End of combination %s" % pformat(combination))
            logging.info("Going to the next in 5s")
            time.sleep(5)

        except Exception as e:
          # Oh no, something goes wrong! Mark combination as cancel for
          # a later retry
            logging.error("Combination %s Failed with message \"%s\". Full exception message:\n%s" % (pformat(combination), e, traceback.format_exc()))
            sweeper.cancel(combination)

        finally:
            pass
        
        

if __name__ == '__main__':
    #Testing
    logging.basicConfig(level=logging.DEBUG)
    perform_experiments()
    #run_experiment(100, "conf.yaml", './temp', destroy=False)
