from copy import deepcopy
from functools import wraps
import logging
import os
import time

from enoslib.api import run_ansible as enos_run_ansible
from enoslib.task import enostask


JUICE_PATH = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
ANSIBLE_PATH = os.path.join(JUICE_PATH, 'ansible')
SYMLINK_NAME = os.path.abspath(os.path.join(os.getcwd(), 'current'))


@enostask()
def run_ansible(playbook, extra_vars=None, tags=None,
                on_error_continue=False, env=None, **kwargs):
    """State combinator for enoslib.api.run_ansible

    Reads the inventory path from the state and then applied and
    returns value of `enoslib.api.run_ansible`.

    """
    inventory = env["inventory"]
    playbooks = [os.path.join(ANSIBLE_PATH, playbook)]

    return enos_run_ansible(playbooks, inventory, extra_vars, tags,
                            on_error_continue)

######################################################################
# Provider deployments
from enoslib.infra.enos_g5k.provider import G5k

@enostask(new=True)
def g5k_deploy(g5k_config, env=None, force_deploy=False, **kwargs):
    provider = G5k(g5k_config)
    roles, networks = provider.init(force_deploy=force_deploy)
    env['roles'] = roles
    env['networks'] = networks
    logging.info('Wait 30 seconds for iface to be ready...')
    time.sleep(30)
    return env, provider
