#!/usr/bin/env python

import os
import logging
import sys
import pprint
import yaml
import json
import operator
import pickle
import time

from subprocess import run, CompletedProcess
from enoslib.api import generate_inventory
from enoslib.task import enostask, _save_env

from utils import run_ansible, g5k_deploy

logging.basicConfig(level=logging.DEBUG)



def get_ip(g5k_address):
    from subprocess import run, PIPE
    ip = run("dig +short %s"%g5k_address, shell=True, stdout=PIPE).stdout.decode('utf-8').strip(' \r\n')
    
    return ip

def get_host_dict(g5k_address):
    return {"address": g5k_address, "ip": get_ip(g5k_address)}



@enostask()
def deploy(nb_db_entries, conf, provider='g5k', force_deployment=True, xp_name=None,
           tags=['provide', 'inventory', 'deploy', 'senddatatodb', 'reconfigure', 'reconfigure2'], env=None, g5k_job=None, destroy=True, working_directory=".",
           **kwargs):
    config = {}

    if isinstance(conf, str):
        # Get the config object from a yaml file
        with open(conf) as f:
            config = yaml.load(f)
    elif isinstance(conf, dict):
        # Get the config object from a dict
        config = conf
    else:
        # Data format error
        raise Exception(
            'conf is type {!r} while it should be a yaml file or a dict'.format(type(conf)))

    env['db'] = config.get('database', 'mariadb')
    env['monitoring'] = config.get('monitoring', True)
    env['config'] = config

    # Claim resources on Grid'5000
    if 'provide' in tags:
        if provider == 'g5k' and 'g5k' in config:
            env['provider'] = 'g5k'
            updated_env, g5k_job = g5k_deploy(config['g5k'], env=xp_name,
                                     force_deploy=force_deployment)
            env.update(updated_env)
        else:
            raise Exception(
                'The provider {!r} is not supported or it lacks a configuration'.format(provider))

    # Generate the Ansible inventory file
    if 'inventory' in tags:
        env['inventory'] = os.path.join(env['resultdir'], 'hosts')
        generate_inventory(env['roles'] , env['networks'],
                           env['inventory'], check_networks=True)
        _save_env(env)

    # Displaying information
    database_machines = [get_host_dict(host.address) for host in env['roles']['database']]
    database_add_machines = [get_host_dict(host.address) for host in env['roles']['database_add']]
    master_machine = database_machines[0]
    workers_marchines = database_machines[1:len(database_machines)]
    registry_machine = get_host_dict(env['roles']['registry'][0].address)
    ceph_use = config['registry']['ceph']
    ceph_mon_host = config['registry']['ceph_mon_host']
    ceph_keyring_path = config['registry']['ceph_keyring']
    ceph_id = config['registry']['ceph_id']
    ceph_rbd = config['registry']['ceph_rbd']
    print("Databases: %s"%str(database_machines))
    print("Additional databases: %s"%str(database_add_machines))
    print("Registry: %s"%str(registry_machine))
    print("Ceph: %s"%str(ceph_mon_host))




    # Deploy the resources, requires both g5k and inventory executions
    total_deploy_time = 0.
    if 'deploy' in tags:
        deploy_start_time  = time.perf_counter()
        run_ansible('deploy.yml', extra_vars={
            'registry':    config['registry'],
            'db':          env['db'],
            'monitoring':  env['monitoring'],
            'enos_action': 'deploy'
            })
        deploy_end_time  = time.perf_counter()
        total_deploy_time = deploy_end_time - deploy_start_time
    
    sql_file_path = working_directory + "/data.sql"
    if 'senddatatodb' in tags:
        if nb_db_entries > 0:
            print("Main: sending database content")
            sql_file_path = working_directory + "/data.sql"
            relative_sql_file_path = working_directory + "/data.sql"
            if working_directory[0] is not '/':
                relative_sql_file_path = "../" + working_directory + "/data.sql"
            command = "cd db_builder/;python3 generate_db.py %d > %s" % (nb_db_entries, relative_sql_file_path)
            completed_process = run(command, shell=True, check=False)
            if completed_process.returncode is not 0:
                raise Exception("Error: Could not generate DB data")
            with open(sql_file_path) as sql_file:
                sql_data=sql_file.read()
            
            run_ansible('senddatatodb.yml', extra_vars={
                'registry':    config['registry'],
                'db':          env['db'],
                'monitoring':  env['monitoring'],
                'enos_action': 'senddatatodb',
                'data': sql_data
                })
        else:
            open(sql_file_path, "a").close() # create an empty file to be sure it is there
            print("Main: not sending database content (0 entries)")

    total_reconf_time = 0.
    if 'reconfigure' in tags:
        reconf_start_time  = time.perf_counter()
        run_ansible('reconfigure.yml', extra_vars={
            'registry':    config['registry'],
            'db':          env['db'],
            'monitoring':  env['monitoring'],
            'enos_action': 'reconfigure'
            })
        reconf_end_time  = time.perf_counter()
        total_reconf_time = reconf_end_time - reconf_start_time
    
    total_reconf2_time = 0.
    if 'reconfigure2' in tags and len(database_add_machines) > 0:
        print("STARTREC2")
        print("STARTREC2", file=sys.stderr)
        reconf2_start_time  = time.perf_counter()
        run_ansible('reconfigure2.yml', extra_vars={
            'registry':    config['registry'],
            'db':          env['db'],
            'monitoring':  env['monitoring'],
            'enos_action': 'reconfigure2'
            })
        reconf2_end_time  = time.perf_counter()
        total_reconf2_time = reconf2_end_time - reconf2_start_time
        print("ENDREC2")
        print("ENDREC2", file=sys.stderr)
    
    total_time = total_deploy_time+total_reconf_time+total_reconf2_time
    print("Total time in seconds: %f (deploy: %f, reconf: %f, reconf2: %f)"%(total_time, total_deploy_time, total_reconf_time, total_reconf2_time))
        
    if destroy:
        g5k_job.destroy()
    return total_time, total_deploy_time, total_reconf_time, total_reconf2_time
    
