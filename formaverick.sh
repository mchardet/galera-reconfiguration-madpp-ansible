read -p "Have you installed pip deps and changed the oargrid IDs in the 5 conf files (Y/N)? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
#     cd aeolus
#     python3 fix_small_experiments.py
#     python3 big_experiments.py
# 
#     cd ../concerto
#     python3 fix_big_experiments.py
# 
#     cd ../ansible
#     python3 small_experiments.py
#     python3 fix_big_experiments.py
# 
#     cd ../overhead_aeolus_nothing
    cd overhead_aeolus_nothing
    python3 small_experiments.py
    python3 big_experiments.py
    python3 dryrun_small_experiments.py
    python3 dryrun_big_experiments.py

    cd ../overhead_concerto_nothing
    python3 small_experiments.py
    python3 big_experiments.py
    python3 dryrun_small_experiments.py
    python3 dryrun_big_experiments.py
    
    cd ../utils/generate_figure
    ./generate_figures.sh
    cp figure1.pdf figure2.pdf ../..
fi
