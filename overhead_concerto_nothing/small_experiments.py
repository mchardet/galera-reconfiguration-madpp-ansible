import logging

from reserve_overhead_nothing_concerto import perform_experiments

if __name__ == '__main__':
    #Testing
    logging.basicConfig(level=logging.DEBUG)
    perform_experiments(
        start_attempt=1,
        nb_attempts=10,
        nbs_nodes=[(3,1),(3,5),(5,0),(10,0)]
    )
