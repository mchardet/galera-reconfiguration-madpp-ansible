from json import load
from statistics import mean

with open("timeresults.json") as f:
    results = load(f)
    max_distance_deploy = results["max_distance_deploy"]
    max_distance_reconf = results["max_distance_reconf"]

print("=== Comparing Concerto performance predictions to actual time ===")
print()
print("Min: we take the maximum time difference between theory and practice and see how much it represents compared to the instance with the minmium eecution time")
print("Average: we take the maximum time difference between theory and practice and see how much it represents compared to an instance of average eecution time")
print()
print("Deployment:")
#sorted_deploy_trials = sorted(results["trials"], key=lambda x:x["theoretical_deploy"])
#min_deploy = sorted_deploy_trials[0]["theoretical_deploy"]
min_deploy = min([x["theoretical_deploy"] for x in results["trials"]])
mean_deploy = mean([x["theoretical_deploy"] for x in results["trials"]])
print("Min:     %fs = %f%%"%(min_deploy, (max_distance_deploy/min_deploy)*100.))
print("Average: %fs = %f%%"%(mean_deploy, (max_distance_deploy/mean_deploy)*100.))


print()
print("Reconfiguration:")
#sorted_reconf_trials = sorted(results["trials"], key=lambda x:x["theoretical_reconf"])
#min_reconf = sorted_reconf_trials[0]["theoretical_reconf"]
min_reconf = min([x["theoretical_reconf"] for x in results["trials"]])
mean_reconf = mean([x["theoretical_reconf"] for x in results["trials"]])
print("Min:     %fs = %f%%"%(min_reconf, (max_distance_reconf/min_reconf)*100.))
print("Average: %fs = %f%%"%(mean_reconf, (max_distance_reconf/mean_reconf)*100.))

if "aeolus_theoretical_deploy" in results["trials"][0]:
    aeolus_sum_p_deploy = 0.
    aeolus_max_p_deploy = 0.
    aeolus_min_p_deploy = 100.
    ansible_sum_p_deploy = 0.
    ansible_max_p_deploy = 0.
    ansible_min_p_deploy = 100.
    aeolus_sum_p_reconf = 0.
    aeolus_max_p_reconf = 0.
    aeolus_min_p_reconf = 100.
    ansible_sum_p_reconf = 0.
    ansible_max_p_reconf = 0.
    ansible_min_p_reconf = 100.
    for t in results["trials"]:
        theoretical_deploy = t["theoretical_deploy"]
        theoretical_reconf = t["theoretical_reconf"]
        aeolus_theoretical_deploy = t["aeolus_theoretical_deploy"]
        aeolus_theoretical_reconf = t["aeolus_theoretical_reconf"]
        ansible_theoretical_deploy = t["ansible_theoretical_deploy"]
        ansible_theoretical_reconf = t["ansible_theoretical_reconf"]
        
        aeolus_p_deploy = 100.*(aeolus_theoretical_deploy-theoretical_deploy)/aeolus_theoretical_deploy
        aeolus_sum_p_deploy += aeolus_p_deploy
        aeolus_max_p_deploy = max(aeolus_max_p_deploy,aeolus_p_deploy)
        aeolus_min_p_deploy = min(aeolus_min_p_deploy,aeolus_p_deploy)
        
        aeolus_p_reconf = 100.*(aeolus_theoretical_reconf-theoretical_reconf)/aeolus_theoretical_reconf
        aeolus_sum_p_reconf += aeolus_p_reconf
        aeolus_max_p_reconf = max(aeolus_max_p_reconf,aeolus_p_reconf)
        aeolus_min_p_reconf = min(aeolus_min_p_reconf,aeolus_p_reconf)
        
        ansible_p_deploy = 100.*(ansible_theoretical_deploy-theoretical_deploy)/ansible_theoretical_deploy
        ansible_sum_p_deploy += ansible_p_deploy
        ansible_max_p_deploy = max(ansible_max_p_deploy,ansible_p_deploy)
        ansible_min_p_deploy = min(ansible_min_p_deploy,ansible_p_deploy)
        
        ansible_p_reconf = 100.*(ansible_theoretical_reconf-theoretical_reconf)/ansible_theoretical_reconf
        ansible_sum_p_reconf += ansible_p_reconf
        ansible_max_p_reconf = max(ansible_max_p_reconf,ansible_p_reconf)
        ansible_min_p_reconf = min(ansible_min_p_reconf,ansible_p_reconf)
    
    print()
    print()
    print("=== Comparing Concerto performance predictions to Aeolus and Ansible pp ===")
    print()
    print("= VS Aeolus =")
    print("Deployment:")
    print("- Min gain: %f%%" % aeolus_min_p_deploy)
    print("- Max gain: %f%%" % aeolus_max_p_deploy)
    print("- Average gain: %f%%" % (aeolus_sum_p_deploy/len(results["trials"])))
    print("Reconfiguration:")
    print("- Min gain: %f%%" % aeolus_min_p_reconf)
    print("- Max gain: %f%%" % aeolus_max_p_reconf)
    print("- Average gain: %f%%" % (aeolus_sum_p_reconf/len(results["trials"])))
    print()
    print("= VS Ansible =")
    print("Deployment:")
    print("- Min gain: %f%%" % ansible_min_p_deploy)
    print("- Max gain: %f%%" % ansible_max_p_deploy)
    print("- Average gain: %f%%" % (ansible_sum_p_deploy/len(results["trials"])))
    print("Reconfiguration:")
    print("- Min gain: %f%%" % ansible_min_p_reconf)
    print("- Max gain: %f%%" % ansible_max_p_reconf)
    print("- Average gain: %f%%" % (ansible_sum_p_reconf/len(results["trials"])))
