import json

with open("timeresults.json") as f:
    file = json.load(f)
    for t in file["trials"]:
        t_sa = t["t_sa"]
        t_sr = t["t_sr"]
        t_su = t["t_su"]
        t_sc = t["t_sc"]
        t_ci1 = t["t_ci1"]
        t_ci2 = t["t_ci2"]
        t_cc = t["t_cc"]
        t_cr = t["t_cr"]
        t_cs1 = t["t_cs1"]
        t_cs2 = t["t_cs2"]
        aeolus_theoretical_deploy = max(t_sa+t_sr,t_cc+t_cr+max(t_ci1+t_ci2,t_sa))
        aeolus_theoretical_reconf = max(t_cs1+t_cs2+t_cr,t_sr+t_cs1+t_su+t_sc)
        ansible_theoretical_deploy = t_sa+t_sr+t_cc+t_cr+t_ci1+t_ci2
        ansible_theoretical_reconf = t_cs1+t_cs2+t_cr+t_sr+t_su+t_sc
        t["aeolus_theoretical_deploy"] = aeolus_theoretical_deploy
        t["aeolus_theoretical_reconf"] = aeolus_theoretical_reconf
        t["ansible_theoretical_deploy"] = ansible_theoretical_deploy
        t["ansible_theoretical_reconf"] = ansible_theoretical_reconf
    output = json.dumps(file, indent='\t')
    print(output)
