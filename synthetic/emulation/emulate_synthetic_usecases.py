import json
import sys
import random

class UseCaseEmulator:
    @staticmethod
    def generate_random_tuple(length, min_value, max_value):
        from random import uniform
        random_list = []
        for _ in range(length):
            random_list.append(uniform(min_value,max_value))
        return tuple(random_list)
    
    @staticmethod
    def add_gains(dictionary):
        concerto_deploy = dictionary["concerto_deploy"]
        concerto_reconf = dictionary["concerto_reconf"]
        aeolus_deploy = dictionary["aeolus_deploy"]
        aeolus_reconf = dictionary["aeolus_reconf"]
        ansible_deploy = dictionary["ansible_deploy"]
        ansible_reconf = dictionary["ansible_reconf"]
        
        gain_to_aeolus_deploy = 100.*(aeolus_deploy-concerto_deploy)/aeolus_deploy
        gain_to_aeolus_reconf = 100.*(aeolus_reconf-concerto_reconf)/aeolus_reconf
        gain_to_ansible_deploy = 100.*(ansible_deploy-concerto_deploy)/ansible_deploy
        gain_to_ansible_reconf = 100.*(ansible_reconf-concerto_reconf)/ansible_reconf
        
        dictionary["gain_to_aeolus_deploy"] = gain_to_aeolus_deploy
        dictionary["gain_to_aeolus_reconf"] = gain_to_aeolus_reconf
        dictionary["gain_to_ansible_deploy"] = gain_to_ansible_deploy
        dictionary["gain_to_ansible_reconf"] = gain_to_ansible_reconf
    
    def get_params_and_results(self, min_value, max_value):
        raise Exception("Not implemented! Should be implemented by child classes!")
    
    
class ServerClientEmulator(UseCaseEmulator):
    
    def emulate_run(self, d_sa, d_sr, d_sc, d_su, d_ci1, d_ci2, d_cc, d_cr, d_cs1, d_cs2):
        to_return = dict()
        
        concerto_deploy = max(d_sa+d_sr,d_cr+max(d_ci2,d_cc+max(d_ci1,d_sa)))
        concerto_reconf = max(d_cs1+d_cs2+d_cr,d_sr+d_cs1+max(d_su,d_sc))
        aeolus_deploy = max(d_sa+d_sr,d_cc+d_cr+max(d_ci1+d_ci2,d_sa))
        aeolus_reconf = max(d_cs1+d_cs2+d_cr,d_sr+d_cs1+d_su+d_sc)
        ansible_deploy = d_sa+d_sr+d_cc+d_cr+d_ci1+d_ci2
        ansible_reconf = d_cs1+d_cs2+d_cr+d_sr+d_su+d_sc
        
        to_return["concerto_deploy"] = concerto_deploy
        to_return["concerto_reconf"] = concerto_reconf
        to_return["aeolus_deploy"] = aeolus_deploy
        to_return["aeolus_reconf"] = aeolus_reconf
        to_return["ansible_deploy"] = ansible_deploy
        to_return["ansible_reconf"] = ansible_reconf
        
        self.add_gains(to_return)
        
        return to_return
    
    
    def get_params_and_results(self, min_value, max_value):
        param_names = ["d_sa", "d_sr", "d_sc", "d_su", "d_ci1", "d_ci2", "d_cc", "d_cr", "d_cs1", "d_cs2"]
        param_vals = self.generate_random_tuple(len(param_names), min_value, max_value)
        param_dict = dict(zip(param_names,param_vals))
        
        run = self.emulate_run(**param_dict)
    
        return param_dict, run
    
    
class ServerDepsEmulator(UseCaseEmulator):
    
    def __init__(self, nb_deps):
        self.nb_deps = nb_deps
    
    def emulate_run(self, d_sa, d_sc, d_sr, d_ss, d_sp, d_di, d_dr, d_du):
        to_return = dict()
        
        concerto_deploy = max(
            max([d_di[i]+d_dr[i] for i in range(self.nb_deps)]),
            d_sr+max([d_sc[i]+max(d_sa,d_di[i]) for i in range(self.nb_deps)])
        )
        concerto_reconf = max(
            max([d_du[i]+d_ss[i]+d_dr[i] for i in range(self.nb_deps)]),
            d_sr+max([d_ss[i]+d_sp[i] for i in range(self.nb_deps)])
        )
        aeolus_deploy = max(
            max([d_di[i]+d_dr[i] for i in range(self.nb_deps)]),
            d_sr+max(
                [d_sc[i]+max(d_sa+sum([d_sc[j] for j in range(0, i)]),d_di[i]) for i in range(self.nb_deps)]
            )
        )
        aeolus_reconf = max(
            max([d_du[i]+sum([d_ss[j] for j in range(0,i+1)])+d_dr[i] for i in range(self.nb_deps)]),
            d_sr+sum([d_ss[i]+d_sp[i] for i in range(self.nb_deps)])
        )
        ansible_deploy = sum([d_di[i] for i in range(self.nb_deps)])+sum([d_dr[i] for i in range(self.nb_deps)]) + d_sr+sum([d_sc[i] for i in range(self.nb_deps)])+d_sa
        ansible_reconf = sum([d_du[i] for i in range(self.nb_deps)])+sum([d_dr[i] for i in range(self.nb_deps)])+sum([d_ss[i] for i in range(self.nb_deps)])+sum([d_sp[i] for i in range(self.nb_deps)])+d_sr
        
        to_return["concerto_deploy"] = concerto_deploy
        to_return["concerto_reconf"] = concerto_reconf
        to_return["aeolus_deploy"] = aeolus_deploy
        to_return["aeolus_reconf"] = aeolus_reconf
        to_return["ansible_deploy"] = ansible_deploy
        to_return["ansible_reconf"] = ansible_reconf
        
        self.add_gains(to_return)
        
        return to_return
    
    
    def get_params_and_results(self, min_value, max_value):
        from random import uniform
        param_dict = {
            "d_sa": uniform(min_value,max_value),
            "d_sc": [uniform(min_value,max_value) for _ in range(self.nb_deps)],
            "d_sr": uniform(min_value,max_value),
            "d_ss": [uniform(min_value,max_value) for _ in range(self.nb_deps)],
            "d_sp": [uniform(min_value,max_value) for _ in range(self.nb_deps)],
            "d_di": [uniform(min_value,max_value) for _ in range(self.nb_deps)],
            "d_dr": [uniform(min_value,max_value) for _ in range(self.nb_deps)],
            "d_du": [uniform(min_value,max_value) for _ in range(self.nb_deps)]
        }
        
        run = self.emulate_run(**param_dict)
    
        return param_dict, run
    
    
class ServerDepsSPMDEmulator(UseCaseEmulator):
    
    def emulate_run(self, d_sa, d_sc, d_sr, d_ss, d_sp, d_di, d_dr, d_du):
        to_return = dict()
        
        concerto_deploy = max(d_di+d_dr,d_sr+d_sc+max(d_sa,d_di))
        concerto_reconf = max(d_du+d_ss+d_dr,d_sr+d_ss+d_sp)
        aeolus_deploy = max(d_di+d_dr,d_sr+d_sc+max(d_sa,d_di))
        aeolus_reconf = max(d_du+d_ss+d_dr,d_sr+d_ss+d_sp)
        ansible_deploy = d_di+d_dr+d_sa+d_sc+d_sr
        ansible_reconf = d_du+d_ss+d_sp+d_sr+d_dr
        
        to_return["concerto_deploy"] = concerto_deploy
        to_return["concerto_reconf"] = concerto_reconf
        to_return["aeolus_deploy"] = aeolus_deploy
        to_return["aeolus_reconf"] = aeolus_reconf
        to_return["ansible_deploy"] = ansible_deploy
        to_return["ansible_reconf"] = ansible_reconf
        
        self.add_gains(to_return)
        
        return to_return
    
    
    def get_params_and_results(self, min_value, max_value):
        param_names = ["d_sa", "d_sc", "d_sr", "d_ss", "d_sp", "d_di", "d_dr", "d_du"]
        param_vals = self.generate_random_tuple(len(param_names), min_value, max_value)
        param_dict = dict(zip(param_names,param_vals))
        
        run = self.emulate_run(**param_dict)
    
        return param_dict, run
    
    
class DepsSPMDEmulator(UseCaseEmulator):
    
    def __init__(self, nb_deps):
        self.nb_deps = nb_deps
    
    def emulate_run(self, d_di, d_dr, d_du):
        to_return = dict()
        
        concerto_deploy = max([d_di[i]+d_dr[i] for i in range(self.nb_deps)])
        concerto_reconf = max([d_du[i]+d_dr[i] for i in range(self.nb_deps)])
        aeolus_deploy = max([d_di[i]+d_dr[i] for i in range(self.nb_deps)])
        aeolus_reconf = max([d_du[i]+d_dr[i] for i in range(self.nb_deps)])
        ansible_deploy = max([d_di[i] for i in range(self.nb_deps)]) + max([d_dr[i] for i in range(self.nb_deps)])
        ansible_reconf = max([d_du[i] for i in range(self.nb_deps)]) + max([d_dr[i] for i in range(self.nb_deps)])
        
        to_return["concerto_deploy"] = concerto_deploy
        to_return["concerto_reconf"] = concerto_reconf
        to_return["aeolus_deploy"] = aeolus_deploy
        to_return["aeolus_reconf"] = aeolus_reconf
        to_return["ansible_deploy"] = ansible_deploy
        to_return["ansible_reconf"] = ansible_reconf
        
        self.add_gains(to_return)
        
        return to_return
    
    
    def get_params_and_results(self, min_value, max_value):
        from random import uniform
        param_dict = {
            "d_di": [uniform(min_value,max_value) for _ in range(self.nb_deps)],
            "d_dr": [uniform(min_value,max_value) for _ in range(self.nb_deps)],
            "d_du": [uniform(min_value,max_value) for _ in range(self.nb_deps)]
        }
        
        run = self.emulate_run(**param_dict)
    
        return param_dict, run



class MinMaxAvgHolder:
    def __init__(self):
        self.min_val = float('inf')
        self.max_val = float('-inf')
        self.min_id = -1
        self.max_id = -1
        self.sum = 0.0
        self.nb_registered = 0
    
    def update(self, value, index):
        self.nb_registered += 1
        self.sum += value
        if value < self.min_val:
            self.min_val = value
            self.min_id = index
        if value > self.max_val:
            self.max_val = value
            self.max_id = index
    
    def get_average(self):
        return self.sum/self.nb_registered
    

def evaluate_with_random_params(function, min_value, max_value):
    from inspect import signature
    from functools import partial
    from random import uniform
    sig = signature(function)
    for _ in range(len(sig.parameters)):
        function = partial(function, uniform(min_value,max_value))
    return function()
    


def emulate_n_runs(use_case_emulator, n, min_random_duration, max_random_duration):
    min_random_duration = float(min_random_duration)
    max_random_duration = float(max_random_duration)
    to_return = {
        "nb_runs": n,
        "min_random_duration": min_random_duration,
        "max_random_duration": max_random_duration,
        "random_distribution": "uniform",
        "runs": [],
        "analysis": dict()
    }
    concerto_deploy_minmax = MinMaxAvgHolder()
    concerto_reconf_minmax = MinMaxAvgHolder()
    aeolus_deploy_minmax = MinMaxAvgHolder()
    aeolus_reconf_minmax = MinMaxAvgHolder()
    ansible_deploy_minmax = MinMaxAvgHolder()
    ansible_reconf_minmax = MinMaxAvgHolder()
    gain_to_aeolus_deploy_minmax = MinMaxAvgHolder()
    gain_to_aeolus_reconf_minmax = MinMaxAvgHolder()
    gain_to_ansible_deploy_minmax = MinMaxAvgHolder()
    gain_to_ansible_reconf_minmax = MinMaxAvgHolder()
    for i in range(n):
        param_dict,run = use_case_emulator.get_params_and_results(min_random_duration, max_random_duration)
        run["parameters"] = param_dict
        concerto_deploy_minmax.update(run["concerto_deploy"], i)
        concerto_reconf_minmax.update(run["concerto_reconf"], i)
        aeolus_deploy_minmax.update(run["aeolus_deploy"], i)
        aeolus_reconf_minmax.update(run["aeolus_reconf"], i)
        ansible_deploy_minmax.update(run["ansible_deploy"], i)
        ansible_reconf_minmax.update(run["ansible_reconf"], i)
        
        gain_to_aeolus_deploy_minmax.update(run["gain_to_aeolus_deploy"], i)
        gain_to_aeolus_reconf_minmax.update(run["gain_to_aeolus_reconf"], i)
        gain_to_ansible_deploy_minmax.update(run["gain_to_ansible_deploy"], i)
        gain_to_ansible_reconf_minmax.update(run["gain_to_ansible_reconf"], i)
        
        to_return["runs"].append(run)
    
    analysis_dict = {
        "absolute_times": {
            "concerto_deploy_min": {
                "time": concerto_deploy_minmax.min_val,
                "run": to_return["runs"][concerto_deploy_minmax.min_id],
            },
            "concerto_deploy_max": {
                "time": concerto_deploy_minmax.max_val,
                "run": to_return["runs"][concerto_deploy_minmax.max_id],
            },
            "aeolus_deploy_min": {
                "time": aeolus_deploy_minmax.min_val,
                "run": to_return["runs"][aeolus_deploy_minmax.min_id],
            },
            "aeolus_deploy_max": {
                "time": aeolus_deploy_minmax.max_val,
                "run": to_return["runs"][aeolus_deploy_minmax.max_id],
            },
            "ansible_deploy_min": {
                "time": ansible_deploy_minmax.min_val,
                "run": to_return["runs"][ansible_deploy_minmax.min_id],
            },
            "ansible_deploy_max": {
                "time": ansible_deploy_minmax.max_val,
                "run": to_return["runs"][ansible_deploy_minmax.max_id],
            },
            "concerto_reconf_min": {
                "time": concerto_reconf_minmax.min_val,
                "run": to_return["runs"][concerto_reconf_minmax.min_id],
            },
            "concerto_reconf_max": {
                "time": concerto_reconf_minmax.max_val,
                "run": to_return["runs"][concerto_reconf_minmax.max_id],
            },
            "aeolus_reconf_min": {
                "time": aeolus_reconf_minmax.min_val,
                "run": to_return["runs"][aeolus_reconf_minmax.min_id],
            },
            "aeolus_reconf_max": {
                "time": aeolus_reconf_minmax.max_val,
                "run": to_return["runs"][aeolus_reconf_minmax.max_id],
            },
            "ansible_reconf_min": {
                "time": ansible_reconf_minmax.min_val,
                "run": to_return["runs"][ansible_reconf_minmax.min_id],
            },
            "ansible_reconf_max": {
                "time": ansible_reconf_minmax.max_val,
                "run": to_return["runs"][ansible_reconf_minmax.max_id],
            }
        },
        "relative_times": {
            "gain_to_aeolus_deploy_min": {
                "gain": gain_to_aeolus_deploy_minmax.min_val,
                "run": to_return["runs"][gain_to_aeolus_deploy_minmax.min_id],
            },
            "gain_to_aeolus_deploy_max": {
                "gain": gain_to_aeolus_deploy_minmax.max_val,
                "run": to_return["runs"][gain_to_aeolus_deploy_minmax.max_id],
            },
            "gain_to_aeolus_deploy_average": gain_to_aeolus_deploy_minmax.get_average(),
            "gain_to_ansible_deploy_min": {
                "gain": gain_to_ansible_deploy_minmax.min_val,
                "run": to_return["runs"][gain_to_ansible_deploy_minmax.min_id],
            },
            "gain_to_ansible_deploy_max": {
                "gain": gain_to_ansible_deploy_minmax.max_val,
                "run": to_return["runs"][gain_to_ansible_deploy_minmax.max_id],
            },
            "gain_to_ansible_deploy_average": gain_to_ansible_deploy_minmax.get_average(),
            "gain_to_aeolus_reconf_min": {
                "gain": gain_to_aeolus_reconf_minmax.min_val,
                "run": to_return["runs"][gain_to_aeolus_reconf_minmax.min_id],
            },
            "gain_to_aeolus_reconf_max": {
                "gain": gain_to_aeolus_reconf_minmax.max_val,
                "run": to_return["runs"][gain_to_aeolus_reconf_minmax.max_id],
            },
            "gain_to_aeolus_reconf_average": gain_to_aeolus_reconf_minmax.get_average(),
            "gain_to_ansible_reconf_min": {
                "gain": gain_to_ansible_reconf_minmax.min_val,
                "run": to_return["runs"][gain_to_ansible_reconf_minmax.min_id],
            },
            "gain_to_ansible_reconf_max": {
                "gain": gain_to_ansible_reconf_minmax.max_val,
                "run": to_return["runs"][gain_to_ansible_reconf_minmax.max_id],
            },
            "gain_to_ansible_reconf_average": gain_to_ansible_reconf_minmax.get_average()
        }
    }
    
    to_return["analysis"] = analysis_dict
    
    return to_return


usage_message = "Syntax: %s <use-case> (<nb-deps>) <runs_output>\n  Use-case: server-client, server-deps, server-deps-spmd, deps-spmd" % sys.argv[0]

if len(sys.argv) < 3:
    print(usage_message, file=sys.stderr)
    exit(1)

use_case = sys.argv[1]

use_cases_map = {
    "server-client": ServerClientEmulator,
    "server-deps": ServerDepsEmulator,
    "server-deps-spmd": ServerDepsSPMDEmulator,
    "deps-spmd": DepsSPMDEmulator,
}

if use_case not in use_cases_map:
    print("Wrong value for use-case parameter.\n%s" % usage_message, file=sys.stderr)
    exit(2)

if use_case == "server-client" or use_case == "server-deps-spmd":
    output_file_path = sys.argv[2]
    use_case_emulator = use_cases_map[use_case]()
else:
    if len(sys.argv) < 4:
        print("The chosen use-case requires an additional parameter.\n%s" % usage_message, file=sys.stderr)
        exit(3)
    nb_deps = int(sys.argv[2])
    output_file_path = sys.argv[3]
    use_case_emulator = use_cases_map[use_case](nb_deps)

random.seed()
results = emulate_n_runs(use_case_emulator, 1000000, 1.0, 100.0)

with open(output_file_path, "w") as f:
    json.dump(results, f, indent='\t')

json_analysis = json.dumps(results["analysis"], indent='\t')
print(json_analysis)
