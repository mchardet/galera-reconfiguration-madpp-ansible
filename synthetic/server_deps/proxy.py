import time

from concerto.all import *
from examples.utils import *

class Proxy(Component):

    def __init__(self):
        self.server_ip = None
        self.my_ip = None
        Component.__init__(self)

    def create(self):
        self.places = [
            'undeployed',
            'allocated',
            'caching',
            'server_running',
            'ready_to_cache'
        ]
        
        self.groups = {
            'use_server_group': ['server_running', 'ready_to_cache'],
            'provide_service_group': ['caching', 'server_running', 'ready_to_cache']
        }

        self.transitions = {
            'deploy': ('undeployed', 'allocated', 'deploy', 0, self.deploy),
            'configure': ('allocated', 'caching', 'deploy', 0, self.configure),
            'use_server': ('caching', 'server_running', 'deploy', 0, self.use_server),
            'prepare_to_cache': ('server_running', 'ready_to_cache', 'cache', 0, self.prepare_to_cache),
            'cache': ('ready_to_cache', 'caching', 'cache', 0, self.cache)
        }

        self.dependencies = {
            'ip': (DepType.DATA_PROVIDE, ['allocated']),
            'service': (DepType.PROVIDE, ['provide_service_group']),
            'server_ip': (DepType.DATA_USE, ['configure']),
            'serviceu': (DepType.USE, ['use_server_group'])
        }
        
        self.initial_place = 'undeployed'

    def deploy(self):
        self.print_color("allocating resources")
        time.sleep(4)
        self.my_ip = "1.2.3.4"
        self.print_color("got IP %s" % self.my_ip)
        self.write('ip', self.my_ip)
        self.print_color("finished allocation, waiting for server's IP")

    def configure(self):
        self.server_ip = self.read('server_ip')
        self.print_color("configuration [server IP: %s]" % self.server_ip)
        time.sleep(1)
        self.print_color("configured")

    def use_server(self):
        self.print_color("passing requests to server")
    
    def prepare_to_cache(self):
        self.print_color("stopping use of server")
        time.sleep(1)

    def cache(self):
        self.print_color("caching requests")
