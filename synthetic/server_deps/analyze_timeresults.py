from json import load
from statistics import mean

with open("timeresults.json") as f:
    results = load(f)
    max_distance_deploy = results["max_distance_deploy"]
    max_distance_reconf = results["max_distance_reconf"]

print("=== Comparing Concerto performance predictions to actual time ===")
print()
print("Min: we take the maximum time difference between theory and practice and see how much it represents compared to the instance with the minmium eecution time")
print("Average: we take the maximum time difference between theory and practice and see how much it represents compared to an instance of average eecution time")
print()
print("Deployment:")
#sorted_deploy_trials = sorted(results["trials"], key=lambda x:x["theoretical_deploy"])
#min_deploy = sorted_deploy_trials[0]["theoretical_deploy"]
min_deploy = min([x["theoretical_deploy"] for x in results["trials"]])
mean_deploy = mean([x["theoretical_deploy"] for x in results["trials"]])
print("Min:     %fs = %f%%"%(min_deploy, (max_distance_deploy/min_deploy)*100.))
print("Average: %fs = %f%%"%(mean_deploy, (max_distance_deploy/mean_deploy)*100.))


print()
print("Reconfiguration:")
#sorted_reconf_trials = sorted(results["trials"], key=lambda x:x["theoretical_reconf"])
#min_reconf = sorted_reconf_trials[0]["theoretical_reconf"]
min_reconf = min([x["theoretical_reconf"] for x in results["trials"]])
mean_reconf = mean([x["theoretical_reconf"] for x in results["trials"]])
print("Min:     %fs = %f%%"%(min_reconf, (max_distance_reconf/min_reconf)*100.))
print("Average: %fs = %f%%"%(mean_reconf, (max_distance_reconf/mean_reconf)*100.))

if "aeolus_theoretical_deploy" in results["trials"][0]:
    print()
    print()
    print("=== Comparing Concerto performance predictions to Aeolus and Ansible pp ===")
    for nb_deps in [1, 5, 10]:
        
        aeolus_sum_p_deploy = 0.
        aeolus_max_p_deploy = 0.
        aeolus_min_p_deploy = 100.
        ansible_sum_p_deploy_spmd = 0.
        ansible_max_p_deploy_spmd = 0.
        ansible_min_p_deploy_spmd = 100.
        ansible_sum_p_deploy_diff = 0.
        ansible_max_p_deploy_diff = 0.
        ansible_min_p_deploy_diff = 100.
        aeolus_sum_p_reconf = 0.
        aeolus_max_p_reconf = 0.
        aeolus_min_p_reconf = 100.
        ansible_sum_p_reconf_spmd = 0.
        ansible_max_p_reconf_spmd = 0.
        ansible_min_p_reconf_spmd = 100.
        ansible_sum_p_reconf_diff = 0.
        ansible_max_p_reconf_diff = 0.
        ansible_min_p_reconf_diff = 100.
        for t in results["trials"]:
            if len(t["t_sc"]) != nb_deps:
                continue
            
            theoretical_deploy = t["theoretical_deploy"]
            theoretical_reconf = t["theoretical_reconf"]
            aeolus_theoretical_deploy = t["aeolus_theoretical_deploy"]
            aeolus_theoretical_reconf = t["aeolus_theoretical_reconf"]
            ansible_theoretical_deploy_spmd = t["ansible_theoretical_deploy_spmd"]
            ansible_theoretical_reconf_spmd = t["ansible_theoretical_reconf_spmd"]
            ansible_theoretical_deploy_diff = t["ansible_theoretical_deploy_diff"]
            ansible_theoretical_reconf_diff = t["ansible_theoretical_reconf_diff"]
            
            aeolus_p_deploy = 100.*(aeolus_theoretical_deploy-theoretical_deploy)/aeolus_theoretical_deploy
            aeolus_sum_p_deploy += aeolus_p_deploy
            aeolus_max_p_deploy = max(aeolus_max_p_deploy,aeolus_p_deploy)
            aeolus_min_p_deploy = min(aeolus_min_p_deploy,aeolus_p_deploy)
            
            aeolus_p_reconf = 100.*(aeolus_theoretical_reconf-theoretical_reconf)/aeolus_theoretical_reconf
            aeolus_sum_p_reconf += aeolus_p_reconf
            aeolus_max_p_reconf = max(aeolus_max_p_reconf,aeolus_p_reconf)
            aeolus_min_p_reconf = min(aeolus_min_p_reconf,aeolus_p_reconf)
            
            ansible_p_deploy_spmd = 100.*(ansible_theoretical_deploy_spmd-theoretical_deploy)/ansible_theoretical_deploy_spmd
            ansible_sum_p_deploy_spmd += ansible_p_deploy_spmd
            ansible_max_p_deploy_spmd = max(ansible_max_p_deploy_spmd,ansible_p_deploy_spmd)
            ansible_min_p_deploy_spmd = min(ansible_min_p_deploy_spmd,ansible_p_deploy_spmd)
            
            ansible_p_reconf_spmd = 100.*(ansible_theoretical_reconf_spmd-theoretical_reconf)/ansible_theoretical_reconf_spmd
            ansible_sum_p_reconf_spmd += ansible_p_reconf_spmd
            ansible_max_p_reconf_spmd = max(ansible_max_p_reconf_spmd,ansible_p_reconf_spmd)
            ansible_min_p_reconf_spmd = min(ansible_min_p_reconf_spmd,ansible_p_reconf_spmd)
            
            ansible_p_deploy_diff = 100.*(ansible_theoretical_deploy_diff-theoretical_deploy)/ansible_theoretical_deploy_diff
            ansible_sum_p_deploy_diff += ansible_p_deploy_diff
            ansible_max_p_deploy_diff = max(ansible_max_p_deploy_diff,ansible_p_deploy_diff)
            ansible_min_p_deploy_diff = min(ansible_min_p_deploy_diff,ansible_p_deploy_diff)
            
            ansible_p_reconf_diff = 100.*(ansible_theoretical_reconf_diff-theoretical_reconf)/ansible_theoretical_reconf_diff
            ansible_sum_p_reconf_diff += ansible_p_reconf_diff
            ansible_max_p_reconf_diff = max(ansible_max_p_reconf_diff,ansible_p_reconf_diff)
            ansible_min_p_reconf_diff = min(ansible_min_p_reconf_diff,ansible_p_reconf_diff)
        
        
        print()
        print("== %d deps =="%nb_deps)
        print()
        print("= VS Aeolus =")
        print("Deployment:")
        print("- Min gain: %f%%" % aeolus_min_p_deploy)
        print("- Max gain: %f%%" % aeolus_max_p_deploy)
        print("- Average gain: %f%%" % (aeolus_sum_p_deploy/len(results["trials"])))
        print("Reconfiguration:")
        print("- Min gain: %f%%" % aeolus_min_p_reconf)
        print("- Max gain: %f%%" % aeolus_max_p_reconf)
        print("- Average gain: %f%%" % (aeolus_sum_p_reconf/len(results["trials"])))
        print()
        print("= VS Ansible SPMD =")
        print("Deployment:")
        print("- Min gain: %f%%" % ansible_min_p_deploy_spmd)
        print("- Max gain: %f%%" % ansible_max_p_deploy_spmd)
        print("- Average gain: %f%%" % (ansible_sum_p_deploy_spmd/len(results["trials"])))
        print("Reconfiguration:")
        print("- Min gain: %f%%" % ansible_min_p_reconf_spmd)
        print("- Max gain: %f%%" % ansible_max_p_reconf_spmd)
        print("- Average gain: %f%%" % (ansible_sum_p_reconf_spmd/len(results["trials"])))
        print()
        print("= VS Ansible DIFF (= seq) =")
        print("Deployment:")
        print("- Min gain: %f%%" % ansible_min_p_deploy_diff)
        print("- Max gain: %f%%" % ansible_max_p_deploy_diff)
        print("- Average gain: %f%%" % (ansible_sum_p_deploy_diff/len(results["trials"])))
        print("Reconfiguration:")
        print("- Min gain: %f%%" % ansible_min_p_reconf_diff)
        print("- Max gain: %f%%" % ansible_max_p_reconf_diff)
        print("- Average gain: %f%%" % (ansible_sum_p_reconf_diff/len(results["trials"])))
        print()
