import json

with open("timeresults.json") as f:
    file = json.load(f)
    for t in file["trials"]:
        t_sa = t["t_sa"]
        t_sc = t["t_sc"]
        t_sr = t["t_sr"]
        t_ss = t["t_ss"]
        t_sp = t["t_sp"]
        t_di = t["t_di"]
        t_dr = t["t_dr"]
        t_du = t["t_du"]
        nb_deps = len(t_du)
        
        deploy_theoretical_time = \
            max(
                max([t_di[i]+t_dr[i] for i in range(nb_deps)]),
                t_sr+max([t_sc[i]+max(t_sa,t_di[i]) for i in range(nb_deps)])
            )
        reconf_theoretical_time = max(max([t_du[i]+t_ss[i]+t_dr[i] for i in range(nb_deps)]),t_sr+max([t_ss[i]+t_sp[i] for i in range(nb_deps)]))
        
        aeolus_theoretical_deploy = \
            max(
                max([t_di[i]+t_dr[i] for i in range(nb_deps)]),
                t_sr+max(
                    [t_sc[i]+max(t_sa+sum([t_sc[j] for j in range(0, i)]),t_di[i]) for i in range(nb_deps)]
                )
            )
        aeolus_theoretical_reconf = max(max([t_du[i]+sum([t_ss[j] for j in range(0,i+1)])+t_dr[i] for i in range(nb_deps)]),t_sr+sum([t_ss[i]+t_sp[i] for i in range(nb_deps)]))
        
        ansible_theoretical_deploy_spmd = max([t_di[i] for i in range(nb_deps)])+max([t_dr[i] for i in range(nb_deps)]) + t_sr+sum([t_sc[i] for i in range(nb_deps)])+t_sa
        ansible_theoretical_deploy_diff = sum([t_di[i] for i in range(nb_deps)])+sum([t_dr[i] for i in range(nb_deps)]) + t_sr+sum([t_sc[i] for i in range(nb_deps)])+t_sa
        
        ansible_theoretical_reconf_spmd = max([t_du[i] for i in range(nb_deps)])+max([t_dr[i] for i in range(nb_deps)])+sum([t_ss[i] for i in range(nb_deps)])+sum([t_sp[i] for i in range(nb_deps)])+t_sr
        ansible_theoretical_reconf_diff = sum([t_du[i] for i in range(nb_deps)])+sum([t_dr[i] for i in range(nb_deps)])+sum([t_ss[i] for i in range(nb_deps)])+sum([t_sp[i] for i in range(nb_deps)])+t_sr
        
        t["aeolus_theoretical_deploy"] = aeolus_theoretical_deploy
        t["aeolus_theoretical_reconf"] = aeolus_theoretical_reconf
        t["ansible_theoretical_deploy_spmd"] = ansible_theoretical_deploy_spmd
        t["ansible_theoretical_reconf_spmd"] = ansible_theoretical_reconf_spmd
        t["ansible_theoretical_deploy_diff"] = ansible_theoretical_deploy_diff
        t["ansible_theoretical_reconf_diff"] = ansible_theoretical_reconf_diff
    output = json.dumps(file, indent='\t')
    print(output)
